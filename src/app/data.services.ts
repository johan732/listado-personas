import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginService } from './login/login.service';
import { Persona } from './persona.model';

@Injectable()
export class DataServices {
  dataSetUrl =
    'https://listado-personas-b403a-default-rtdb.firebaseio.com/datos.json';

  constructor(
    private httpClient: HttpClient,
    private loginService: LoginService
  ) {}

  // Cargar personas
  cargarPersonas() {
    const token = this.loginService.getIdToken();
    return this.httpClient.get(this.dataSetUrl + '?auth=' + token);
  }

  // Guardar personas
  guardarPersonas(personas: Persona[]) {
    const token = this.loginService.getIdToken();

    this.httpClient.put(this.dataSetUrl + '?auth=' + token, personas).subscribe(
      (response) => console.log('Resultado guardar personas ' + response),
      (error) => console.log('Error al guardar personas ' + error)
    );
  }

  modificarPersona(index: number, persona: Persona) {
    const token = this.loginService.getIdToken();
    let url: string;
    url =
      'https://listado-personas-b403a-default-rtdb.firebaseio.com/datos/' +
      index +
      '.json' +
      '?auth=' +
      token;
    this.httpClient.put(url, persona).subscribe(
      (response) => console.log('Resultado modificar Persona' + response),
      (error) => console.log('Error en modificar persona: ' + error)
    );
  }

  eliminarPersona(index: number) {
    const token = this.loginService.getIdToken();
    let url: string;
    url =
      'https://listado-personas-b403a-default-rtdb.firebaseio.com/datos/' +
      index +
      '.json' +
      '?auth=' +
      token;
    this.httpClient.delete(url).subscribe(
      (response) => console.log('Resultado eliminar Persona' + response),
      (error) => console.log('Error en eliminar persona: ' + error)
    );
  }
}
