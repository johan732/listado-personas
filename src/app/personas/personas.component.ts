import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogginService } from '../LoggingService.service';
import { Persona } from '../persona.model';
import { PersonasService } from '../personas.service';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css'],
})
export class PersonasComponent implements OnInit {
  personas: Persona[] = [];

  constructor(
    private personasService: PersonasService,
    private router: Router
  ) {}

  //Este método se ejecuta después del constructor
  ngOnInit(): void {
    //this.personas = this.personasService.personas;
    this.personasService.obtenerPersonas().subscribe((personas: any) => {
      this.personas = <Persona[]>personas;
      this.personasService.setPersonas(this.personas);
    });
  }

  agregar() {
    this.router.navigate(['personas/agregar']);
  }
}
