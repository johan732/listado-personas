import { EventEmitter, Injectable } from '@angular/core';
import { DataServices } from './data.services';
import { LogginService } from './LoggingService.service';
import { Persona } from './persona.model';

@Injectable()
export class PersonasService {
  personas: Persona[] = [];

  saludar = new EventEmitter<number>();

  // Inyectamos el servicio del dataservice para registrar en la BD
  constructor(
    private logginService: LogginService,
    private dataServices: DataServices
  ) {}

  // Actualizar la lista de personas
  setPersonas(personas: Persona[]) {
    this.personas = personas;
  }

  // Método que consulta el service de la BD para cargar las personas
  obtenerPersonas() {
    return this.dataServices.cargarPersonas();
  }

  agregarPersona(persona: Persona) {
    if (this.personas == null) {
      this.personas = [];
    }

    this.personas.push(persona);
    this.logginService.enviarMensajeAConsola(
      'Agregamos persona: ' + persona.nombre
    );

    // agregamos el método a esta función para guardar en BD
    this.dataServices.guardarPersonas(this.personas);
  }

  encontrarPersona(index: number) {
    let persona: Persona = this.personas[index];
    return persona;
  }

  modificarPersona(index: number, persona: Persona) {
    let persona1: Persona = this.personas[index];
    persona1.nombre = persona.nombre;
    persona1.apellido = persona.apellido;

    this.dataServices.modificarPersona(index, persona);
  }

  eliminarPersona(index: number) {
    this.personas.splice(index, 1);
    this.dataServices.eliminarPersona(index);
    // cargar nuevamente los datos para regenerar los indices
    this.modificarPersonas();
  }

  /** Para actualizar los indices en la BD */
  modificarPersonas() {
    if (this.personas != null) {
      this.dataServices.guardarPersonas(this.personas);
    }
  }
}
