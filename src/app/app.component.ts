import { Component, OnInit } from '@angular/core';
import firebase from 'firebase';
import { LoginService } from './login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  titulo = 'Listado de personas';

  constructor(private loginService: LoginService) {}

  ngOnInit(): void {
    firebase.initializeApp({
      apiKey: 'AIzaSyDHsqihtIIJiuIdh_2mJwrk985Yb9DIDWk',
      authDomain: 'listado-personas-b403a.firebaseapp.com',
    });
  }

  isAutenticado() {
    return this.loginService.isAutenticado();
  }
  salir() {
    this.loginService.logout();
  }
}
